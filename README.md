Pydev is a self contained Python development environment in Alpine Linux. It's raison d'etre is for a sandboxed python service/task docker creation kit, so everyone on our dev team uses the same development tools and libraries.

This is no small image. It will create a Pipfile to track prerequisites, a Dockerfile and a docker-compose.yaml to build/run your service, and even give you a main.py if you didn't provide it with existing code.

The file structure it builds looks like:
 - /project/
   - code/
     - main.py
   - Dockerfile
   - Pipfile
   - docker-compose.yaml

It uses:
 - python 3
 - pipenv
 - docker
 - vim

it also includes, in case you need them:
 - npm
 - gcc
 - postgresql-dev
 - mariadb-dev
 - sqlite

Upon run, you can supply the following environment variables:
 - GIT_SOURCE = remote respository to clone
 - GIT_USERNAME = Your name, to put in `git config user.name`
 - GIT_USEREMAIL = Your email, to put in `git config user.email`
 - GIT_TEMPLATE_REPO = After initializing a new repository, it will perform a squashed pull on this repository.
 - GIT_TEMPLATE_BRANCH = for specifying what branch to use from GIT_TEMPLATE_REPO, 'master' by default.

You can specify any other environment variables you might need, such as DJANGO_SETTINGS_MODULE.

The following volume mounts are available:
 - /project = where you will be coding, bind your local repo here
 - /home/.gitconfig = you can optionally bind your own ~/.gitconfig here
 - /home/.vim = you can optionally bind your .vim folder here, everyone has their preferences.
 - /home/.vimrc = if you do above, don't forget your vimrc ;)
 - /home/.ssh = if you use ssh keys to authenticate, this is probably a good idea.


If you are making a web project such as django, you will probably want to remember to bind a local port to your project's dev web port. (EG, 8000 on django)

An example run looks kind of like:
```bash
docker run --rm -it \
            -v .:/project \
            -v ~/.ssh:/home/ssh \
            -v ~/.vim:/home/.vim \
            -v ~/.vimrc:/home/.vimrc \
            -p 8000:8000 \
            cwrinn/pydev
```
It might behoove you to save the above as a bash function or script. It gets verbose quickly.
