#!/bin/bash
command="$@"

if [[ -z "${command}" ]];then

  if [[ -n "${GIT_SOURCE}" ]];then
    git clone "${GIT_SOURCE}" . || exit 1
  fi
  
  if [[ ! -f Pipfile ]];then
    pipenv --python ${PYTHON_VERSION}
  fi
  
  if [[ ! -f Dockerfile ]];then
    sh /templates/Dockerfile.tpl
  fi
  
  if [[ ! -f docker-compose.yaml ]];then
    sh /templates/docker-compose.tpl
  fi
  
  if [[ ! -d code ]];then
  (
    mkdir -p code
    cd code
    if [[ ! -f main.py ]];then
      sh /templates/main.tpl
    fi
  )
  fi


  if [[ -n "${GIT_USERNAME}" ]];then
    git config user.name "${GIT_USERNAME}"
  fi

  if [[ -n "${GIT_USEREMAIL}" ]];then
    git config user.email "${GIT_USEREMAIL}"
  fi
 
  if git config user.name &> /dev/null\
  && git config user.email &> /dev/null;then
    if [[ ! -d .git ]];then
      git init
      git add .
      git commit -m "Initial Commit."
      if [[ -n "${GIT_TEMPLATE_REPO}" ]];then
        git pull --squash -b "${GIT_TEMPLATE_BRANCH:-master}"\
                             "${GIT_TEMPlATE_REPO}" &&\
        git commit -m "Adding ${GIT_TEMPLATE_REPO} in a squashed commit as a template."
      fi
    fi
  else
    echo '
Git does not appear to be setup.
Did you forget mount your ~/.gitconfig
or set GIT_USERNAME and GIT_USEREMAIL?

To setup your repository, run:
  git config user.name "Your Name"
  git config user.email "your@email.com"
  git add .
  git commit -m "Initial Commit."'
  fi


  pipenv install --dev 
  export VIRTUAL_ENV_DISABLE_PROMPT=1
  command="/usr/local/bin/pipenv shell"
fi

exec ${command}
