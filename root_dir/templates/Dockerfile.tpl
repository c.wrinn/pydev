cat >Dockerfile <<EOF
FROM python:${PYTHON_VERSION}-alpine

WORKDIR /app
ADD Pipfile /app/Pipfile

RUN pip install pipenv --upgrade\
 && pipenv install
ADD . /app

CMD ["pipenv", "run", "main.py"]
EOF
